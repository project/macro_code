<?php
  /**
   * Available variables:
   *  $form   - the rendered form output;
   *  $state  - the current recording state (one of: paused, recording, or empty);
   */
?>

<div id="mc-wrapper">
	<div id="mc-body">
	  <?php echo $form; ?>
	</div> 
	<div id="mc-control" class="<?php echo $state; ?>">
	  <?php echo l(t('« Macro control'), '', array('attributes' => array('class' => 'visibility-toggle'))); ?>
	</div>
</div>