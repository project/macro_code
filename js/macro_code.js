var mc = window.mc || {};

mc = {
  init: function()
  {
    if('#mc-body') {
      mc.popup();
    }
    
    if('#mc-details') {
      mc.details();
    }
  },
  
  popup: function()
  {
    $('#mc-control .visibility-toggle').click(function(e){
      e.preventDefault();
      
      var speed = 500;
      
      if($('#mc-body').css('display') == 'block') {
        $('#mc-body').fadeOut(speed);
      }
      else {
        $('#mc-body').fadeIn(speed);
      }
    });
  },
  
  details: function()
  {
    $('.mc-detail-trigger').each(function(i, el) {
      $(this).click(function(e){
        e.preventDefault();
				$('#mc-details').load(this.href);  
      });
    });
  }
}

if(Drupal.jsEnabled)
{
  $(document).ready(mc.init);
  
  $(document).ready(
    // mc.init;
    function() {
    
    var base_path = location.protocol + '//' + location.hostname + Drupal.settings.basePath;

    // Add detail function
    if(Drupal.settings.macroCodeDetail){
      $('#macro-code-import span.macro-detail').click(function(){
        if($(this).hasClass('open')){
          $(this).next('.macro-detail-container').slideUp();
          $(this).addClass('closed').text('Details').removeClass('open');
        }
        else if($(this).hasClass('closed')){
          $(this).next('.macro-detail-container').slideDown();
          $(this).addClass('open').text('Hide Details').removeClass('closed');
        }
        else{
          var macro = $(this).attr('rel');
          $(this).next('.macro-detail-container').load(base_path + 'macro/code/details/' + macro);
          $(this).addClass('open').text('Hide Details');          
        }
        return false;
      });
    }
  });
  // $(document).ready(mc.init);
}
