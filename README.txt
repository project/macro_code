The Macro Code module extends the Macro module to provide automatic exporting
of macros to "component" files. A component file is simply a .macro file with
one or more macros. Component files should be used to group form submissions
whenever it makes sense.

The purpose of keeping form macros (form submissions) in files is to provide
an easy way of duplicating settings (database) changes between multiple
copies of the same project (such as development, staging & production
environments).

This helper module is not intended to duplicate the functionaly provided by
modules such as Features, but rather provide a "catch-all" solution for
form submissions that Features (and similar modules) do not support. 

The intended workflow is as follows:
- make form changes until you are satisfied with the result
- create a component file and "resume recording" on it
- re-submit all forms which contain data required for that piece of
functionality
- stop recording
- commit component file to version control
- pull in the component file on another server and import it (if this is the
production environment, the component file can also be deleted from version
control at this point)